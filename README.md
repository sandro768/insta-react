# Insta Frontend React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Run the following command and have some fun 🤘

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### While using APP

In case you wanna logout just clear the localStorage. 🧹

The server-side validation is working but has no effect on UI so, please keep the browser console opened, so you can
observe all the processes going on 🔬

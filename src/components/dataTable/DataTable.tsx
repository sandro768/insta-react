import React from 'react';
import {
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  CircularProgress,
  Paper,
  Box,
  IconButton,
  Typography,
} from '@material-ui/core';
import SaveOutlinedIcon from '@material-ui/icons/SaveOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { isEmpty, map } from 'lodash-es';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      marginTop: theme.spacing(2),
      maxHeight: 440,
    },
  }),
);

type Props = {
  data: any,
  title: string,
  loading: boolean,
  clickAction: Function,
}

export type RowStrings = {
  id: string,
  picture: string,
  username: string,
  mediaCount: string,
  type: string,
}

const DataTable = ({data, title, loading, clickAction}: Props) => {
  const classes = useStyles();
  let action = null;

  return (
    !isEmpty(data) ? (
      <TableContainer component={Paper} className={classes.container}>
        <Typography variant='h4'>{title}</Typography>
        <Table stickyHeader aria-label="table">
          <TableHead>
            <TableRow>
              <TableCell>Image</TableCell>
              <TableCell align="right">Type</TableCell>
              <TableCell align="right">Username/Name</TableCell>
              <TableCell align="right">Media count</TableCell>
              <TableCell align="right">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {map(data, (row) => {
                action = (
                  <IconButton disabled={loading}
                              onClick={() => clickAction(row.rowStrings)}>
                    <SaveOutlinedIcon />
                  </IconButton>
                );
                if (row.user) {
                  row.rowStrings = {
                    id: row.user.pk,
                    picture: row.user.profile_pic_url,
                    username: row.user.username,
                    mediaCount: '',
                    type: 'User',
                  };
                } else if (row.hashtag) {
                  row.rowStrings = {
                    id: row.hashtag.id,
                    picture: row.hashtag.profile_pic_url,
                    username: row.hashtag.name,
                    mediaCount: row.hashtag.media_count,
                    type: 'Hashtag',
                  };
                } else {
                  row.rowStrings = {
                    id: row._id,
                    ...row,
                  };
                  action = (
                    <IconButton disabled={loading}
                                onClick={() => clickAction(row.rowStrings.id)}>
                      <DeleteIcon />
                    </IconButton>
                  );
                }

                return (
                  <TableRow key={row.rowStrings.id}>
                    <TableCell component="th" scope="row">
                      <img width={40} src={row.rowStrings.picture} alt="" />
                    </TableCell>
                    <TableCell align="right">{row.rowStrings.type}</TableCell>
                    <TableCell
                      align="right">{row.rowStrings.username}</TableCell>
                    <TableCell
                      align="right">{row.rowStrings.mediaCount}</TableCell>
                    <TableCell align="right">
                      {action}
                    </TableCell>
                  </TableRow>
                );
              },
            )}
          </TableBody>
        </Table>
      </TableContainer>
    ) : loading ? (
      <Box display="flex" justifyContent="center" my={2}>
        <CircularProgress size={80} />
      </Box>
    ) : null
  );
};

export default DataTable;

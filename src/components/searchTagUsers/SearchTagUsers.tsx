import React, { ChangeEvent, FormEvent } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      padding: '2px 4px',
      display: 'flex',
      width: 600,
      margin: 'auto',
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
  }),
);

type Props = {
  searchChange: (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => void,
  search: ((event: FormEvent<HTMLDivElement>) => void) | undefined
}

const SearchTagUsers = ({searchChange, search}: Props) => {
  const classes = useStyles();

  return (
    <Paper component="form" className={classes.root} onSubmit={search}>
      <InputBase
        className={classes.input}
        onChange={searchChange}
        placeholder="Search"
        inputProps={{'aria-label': 'search'}}
      />
      <IconButton type="submit" className={classes.iconButton}
                  aria-label="search">
        <SearchIcon />
      </IconButton>
    </Paper>
  );
};

export default SearchTagUsers;

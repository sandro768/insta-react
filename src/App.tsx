import React, { useEffect, useState } from 'react';
import { Route, Switch, useHistory } from 'react-router';
import SignIn from './pages/auth/SignIn';
import SignUp from './pages/auth/SignUp';
import Insta from './pages/insta/Insta';

const App = () => {
  const history = useHistory();
  const [token, setToken] = useState(localStorage.getItem('token'));
  const notAuthenticated = !token;
  useEffect(() => {
    if (notAuthenticated) {
      history.push('/sign-in');
    }
  }, [history, notAuthenticated]);
  return (
    <div>
      <Switch>
        {notAuthenticated ? (
          <>
            <Route path="/sign-in">
              <SignIn updateToken={(token: string) => setToken(token)} />
            </Route>
            <Route path="/sign-up">
              <SignUp />
            </Route>
          </>
        ) : (
          <Route path="/">
            <Insta />
          </Route>
        )}
      </Switch>
    </div>
  );
};

export default App;

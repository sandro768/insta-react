import React, { useState, ChangeEvent, FormEvent } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { validate, FormInputs } from './helper';
import axios from 'axios';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

type Props = {
  updateToken: Function
}

const SignIn = ({updateToken}: Props) => {
  const classes = useStyles();
  const history = useHistory();

  const [inputs, setInputs] = useState<FormInputs>({
    email: {
      value: '',
      errorMessage: '',
      rules: {
        required: true,
        isEmail: true,
      },
    },
    password: {
      value: '',
      errorMessage: '',
      rules: {
        required: true,
      },
    },
  });

  const updateInputValueHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    element: 'email' | 'password') => {
    setInputs(prevState => ({
      ...prevState,
      [element]: {
        ...prevState[element],
        value: event.target.value,
      },
    }));
  };

  const signInHandler = (event: FormEvent<HTMLFormElement>) => {
    const {updatedInputs, errors} = validate(event, inputs);
    // proceed if no errors
    setInputs(updatedInputs);
    if (errors) {
      return;
    }
    axios.post('http://localhost:8080/auth/signin', {
      email: inputs.email.value,
      password: inputs.password.value,
    }).then(response => {
      localStorage.setItem('token', response.data.token);
      updateToken(response.data.token);
      history.push('/');
    }).catch(err => {
      console.log(err);
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate
              onSubmit={(event => signInHandler(event))}>
          <TextField
            onChange={(event) => updateInputValueHandler(event, 'email')}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            helperText={inputs.email.errorMessage}
            error={inputs.email.errorMessage.length > 0}
          />
          <TextField
            onChange={(event) => updateInputValueHandler(event, 'password')}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            helperText={inputs.password.errorMessage}
            error={inputs.password.errorMessage.length > 0}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item>
              <Link to="/sign-up">
                {'Don\'t have an account? Sign Up'}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default SignIn;

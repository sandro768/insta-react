import { FormEvent } from 'react';
import { forEach } from 'lodash-es';

export type FormInputs = {
  [key: string]: {
    value: string,
    errorMessage: string,
    rules: {
      required?: boolean,
      isEmail?: boolean
    }
  },
}

type ObjectLiteral = {
  [key: string]: any;
}

export const checkValidity = (value: string, rules: ObjectLiteral) => {
  let isValid = true;

  if (rules.required && isValid) {
    isValid = value.trim() !== '';
  }

  if (rules.isEmail) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value) && isValid;
  }

  return isValid;
};

export const validate = (event: FormEvent<HTMLFormElement>, inputs: FormInputs) => {
  event.preventDefault();
  const updatedInputs = {...inputs};
  forEach(updatedInputs, (input, key: string) => {
    const valid = checkValidity(input.value, input.rules);
    if (valid) {
      updatedInputs[key].errorMessage = '';
    } else {
      if (key === 'email') {
        updatedInputs.email.errorMessage = 'Valid email is required';
      } else if (key === 'password') {
        updatedInputs.password.errorMessage = 'Password is required';
      } else if (key === 'name') {
        updatedInputs.name.errorMessage = 'Name is required';
      }
    }
  });

  let errors = false;
  forEach(updatedInputs, input => {
    if (input.errorMessage.length > 0) {
      return errors = true;
    }
  });

  return {updatedInputs, errors};
};


import React, { ChangeEvent, FormEvent, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { validate, FormInputs } from './helper';
import axios from 'axios';
import { useHistory } from 'react-router';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const SignUp = () => {
  const classes = useStyles();
  const history = useHistory();

  const [inputs, setInputs] = useState<FormInputs>({
    email: {
      value: '',
      errorMessage: '',
      rules: {
        required: true,
        isEmail: true,
      },
    },
    password: {
      value: '',
      errorMessage: '',
      rules: {
        required: true,
      },
    },
    name: {
      value: '',
      errorMessage: '',
      rules: {
        required: true,
      },
    },
  });

  const updateInputValueHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    element: 'email' | 'password' | 'name') => {
    setInputs(prevState => ({
      ...prevState,
      [element]: {
        ...prevState[element],
        value: event.target.value,
      },
    }));
  };

  const signupInHandler = (event: FormEvent<HTMLFormElement>) => {
    const {updatedInputs, errors} = validate(event, inputs);
    // proceed if no errors
    setInputs(updatedInputs);
    if (errors) {
      return;
    }
    axios.put('http://localhost:8080/auth/signup', {
      email: inputs.email.value,
      password: inputs.password.value,
      name: inputs.name.value,
    }).then(response => {
      history.push('/sign-in');
    }).catch(err => {
      console.log(err);
    });
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate
              onSubmit={(event => signupInHandler(event))}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                onChange={(event) => updateInputValueHandler(event, 'name')}
                variant="outlined"
                required
                fullWidth
                id="name"
                label="Full Name"
                name="name"
                autoComplete="name"
                helperText={inputs.name.errorMessage}
                error={inputs.name.errorMessage.length > 0}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                onChange={(event) => updateInputValueHandler(event, 'email')}
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                helperText={inputs.email.errorMessage}
                error={inputs.email.errorMessage.length > 0}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                onChange={(event) => updateInputValueHandler(event, 'password')}
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                helperText={inputs.password.errorMessage}
                error={inputs.password.errorMessage.length > 0}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to="/sign-in">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
};

export default SignUp;

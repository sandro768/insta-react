import React, { ChangeEvent, FormEvent, useEffect, useState } from 'react';
import { Container } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import axios from 'axios';
import SearchTagUsers from '../../components/searchTagUsers/SearchTagUsers';
import DataTable, { RowStrings } from '../../components/dataTable/DataTable';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      marginTop: theme.spacing(2),
    },
  }),
);

const Insta = () => {
  const classes = useStyles();
  const [search, setSearch] = useState('');
  const [loading, setLoading] = useState(false);
  const [usersHashtags, setUsersHashtags] = useState({});
  const [usersHashtagsDB, setUsersHashtagsDB] = useState([]);

  useEffect(() => {
    setLoading(true);
    axios.get(
      `http://localhost:8080/insta/all`, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })
      .then(response => {
        setLoading(false);
        setUsersHashtagsDB(response.data.result);
      })
      .catch(err => {
        console.log(err);
      });
  }, []);

  const handleSearchChange = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  const handleSearch = async (event: FormEvent<HTMLDivElement>) => {
    setLoading(true);
    event.preventDefault();
    const response = await axios.get(
      `https://www.instagram.com/web/search/topsearch/?context=blended&query=${search}&rank_token=0.590185081828652&include_reel=true`);
    setLoading(false);
    setUsersHashtags(
      response.data.hashtags.concat(response.data.users));
  };

  const handleSave = async (rowStrings: RowStrings) => {
    setLoading(true);
    try {
      const response = await axios.post(`http://localhost:8080/insta/save`, {
        picture: rowStrings.picture,
        username: rowStrings.username,
        mediaCount: rowStrings.mediaCount,
        type: rowStrings.type,
      }, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      });
      setLoading(false);
      setUsersHashtagsDB(response.data.insta);
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  };

  const handleDelete = async (id: string) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `http://localhost:8080/insta/delete/${id}`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        });
      setLoading(false);
      setUsersHashtagsDB(response.data.insta);
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  };

  return (
    <Container className={classes.container}>
      <SearchTagUsers searchChange={handleSearchChange} search={handleSearch} />
      <DataTable
        loading={loading}
        data={usersHashtags}
        title="Save the entries you'd like to"
        clickAction={handleSave} />
      <DataTable
        loading={loading}
        data={usersHashtagsDB}
        title="Entries from the DB"
        clickAction={handleDelete} />
    </Container>
  );
};

export default Insta;
